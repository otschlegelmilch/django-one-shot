from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

# from django.views.generic.edit import CreateView, DeleteView, UpdateView

# from django.shortcuts import render

from todos.models import TodoList

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todo/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todo/detail.html"
